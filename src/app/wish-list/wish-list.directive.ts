import {Directive, HostListener} from '@angular/core';
import {WishListComponent} from './wish-list.component';

@Directive({
    selector: '[appWishList]'
})
export class WishListDirective {

    constructor(private hostComponent: WishListComponent) {
    }

    @HostListener('window:beforeunload', ['$event'])
    beforeUnloadHander() {
        if (this.hostComponent.wishListArray.length > 0) {
            localStorage.setItem('names', JSON.stringify(this.hostComponent.wishListArray));
        }
    }

}
