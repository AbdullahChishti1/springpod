import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-wish-list',
    templateUrl: './wish-list.component.html',
    styleUrls: ['./wish-list.component.css']
})
export class WishListComponent implements OnInit {

    listTitle: string;
    wishListArray = [];
    @Output() close: EventEmitter<any> = new EventEmitter();
    @Output() emitWishList: EventEmitter<any> = new EventEmitter();


    constructor() {
    }

    ngOnInit() {
        let items = JSON.parse(localStorage.getItem('names'));
        if (items) {
            items = items.filter((item) => {
                return item.length !== 0;
            });
        }

        if (this.isArray(items)) {
            for (const item of items) {
                this.wishListArray.push(item);
            }
        } else {
            if (items) {
                const tester = items ? items.toString() : undefined;
                this.wishListArray.push(tester);
            }
        }
        /* populate wishlist from localStorage and emit so other lists can be populated accordingly */
        this.emitWishList.emit((this.wishListArray));
    }


    onDrop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data,
                event.previousIndex,
                event.currentIndex);
        } else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex, event.currentIndex);
        }
    }

    deleteItem(event) {
        this.updateLocalStorage(event);
        console.log(this.wishListArray);
        this.wishListArray = this.wishListArray.filter(items => !items.includes(event));
        this.close.emit(event);
    }

    clearAll(items) {
        this.wishListArray = [];
        localStorage.removeItem('names');
        this.wishListArray = this.wishListArray.filter(item => !item.includes(event));
        this.close.emit(items);
    }


    /**************** private methods ****************/
    private isArray(items): boolean {
        if (items) {
            return items.length > 1 && Array.isArray(items);
        }
    }

    private updateLocalStorage(event) {
        if (event.length > 0) {
            let wishListItems = JSON.parse(localStorage.getItem('names'));
            if (wishListItems) {
                /* removing item from localStorage which has been deleted */
                wishListItems = wishListItems.filter((i) => {
                    return !event.includes(i);
                });
                if (wishListItems.length < 1) {
                    this.wishListArray = [];
                    localStorage.removeItem('names');
                } else {
                    localStorage.setItem('names', JSON.stringify(wishListItems));
                }
            }
        }
    }


}
