import {TestBed} from '@angular/core/testing';

import {GetItemsService} from './get-items.service';
import {HttpClientModule} from '@angular/common/http';

describe('GetItemsService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule]
    }));

    it('should be created', () => {
        const service: GetItemsService = TestBed.get(GetItemsService);
        expect(service).toBeTruthy();
    });
});
