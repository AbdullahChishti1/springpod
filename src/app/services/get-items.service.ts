import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class GetItemsService {
    baseURl = 'http://localhost:3000';

    constructor(private http: HttpClient) {
    }

    getItems(item: string) {
        return this.http.get(this.baseURl + item);
    }

}
