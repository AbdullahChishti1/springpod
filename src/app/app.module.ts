import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MatCardModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {SharedComponent} from './shared/shared.component';
import {WishListComponent} from './wish-list/wish-list.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { WishListDirective } from './wish-list/wish-list.directive';

@NgModule({
    declarations: [
        AppComponent,
        SharedComponent,
        WishListComponent,
        WishListDirective
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatCardModule,
        DragDropModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    entryComponents: [SharedComponent, WishListComponent]
})

export class AppModule {
}
