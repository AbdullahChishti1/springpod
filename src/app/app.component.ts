import {
    AfterContentChecked,
    AfterContentInit,
    Component,
    ComponentFactoryResolver,
    ComponentRef,
    OnInit,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {SharedComponent} from './shared/shared.component';
import {WishListComponent} from './wish-list/wish-list.component';
import {Observable} from 'rxjs';
import {GetItemsService} from './services/get-items.service';
import {filter} from 'rxjs/operators';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterContentInit, AfterContentChecked {


    /* creating ContainerRefs to load components dynamically */
    @ViewChild('foodComponent', {read: ViewContainerRef}) foodComponent: ViewContainerRef;
    @ViewChild('beachComponent', {read: ViewContainerRef}) beachComponent: ViewContainerRef;
    @ViewChild('wishListComponent', {read: ViewContainerRef}) wishListComponent: ViewContainerRef;

    FoodComponent: ComponentRef<SharedComponent>;
    BeachComponent: ComponentRef<SharedComponent>;
    WishlistComponent: ComponentRef<WishListComponent>;
    FoodTitle = 'Food items';
    BeachTitle = 'Beach items';
    WishListTitle = 'Wish list';
    users$: Observable<any[]>;
    foods: any;
    beach: any;
    foodURL = '/FOOD';
    beachURL = '/BEACH';
    foodReplica: string[];
    beachReplica: string[];
    wishListToSend = [];

    constructor(private resolver: ComponentFactoryResolver, private getItemsService: GetItemsService) {
    }


    ngOnInit(): void {
        this.getFoodData().subscribe((FoodData: string[]) => {
            this.foods = FoodData;
            this.foodReplica = [...FoodData];
            this.foodOnReload();
        });

        this.getBeachData().subscribe((FoodData: string[]) => {
            this.beach = FoodData;
            this.beachReplica = [...FoodData];
            this.BeachOnReload();
        });
    }

    ngAfterContentChecked(): void {
        this.FoodComponent.instance.items = this.foods;
        this.BeachComponent.instance.items = this.beach;
    }


    ngAfterContentInit(): void {

        /*factory for our shared component*/
        const sharedFormFactory = this.resolver.resolveComponentFactory(SharedComponent);
        /* factory for our wish list*/

        const wishlistFactory = this.resolver.resolveComponentFactory(WishListComponent);

        /* creating instances of those components */
        this.FoodComponent = this.foodComponent.createComponent(sharedFormFactory);
        this.BeachComponent = this.beachComponent.createComponent(sharedFormFactory);
        this.WishlistComponent = this.wishListComponent.createComponent(wishlistFactory);

        /* sending them to dynamic components */
        this.FoodComponent.instance.title = this.FoodTitle;

        /* for wish list*/
        this.WishlistComponent.instance.wishListArray = this.wishListToSend;
        this.WishlistComponent.instance.listTitle = this.WishListTitle;

        /* for beach */
        this.BeachComponent.instance.title = this.BeachTitle;
        this.WishlistComponent.instance.close.subscribe((items) => {
            this.putDataBack(items);
        });

        this.WishlistComponent.instance.emitWishList.subscribe((wishListItems) => {
            this.filterItems(wishListItems);
        });

    }

    filterItems(filterItems): Observable<any> {
        return this.users$ = filterItems;
    }

    /* method puts data back to the original arrays after delete or clear all is clicked */
    putDataBack(event) {
        if (event.length > 1 && Array.isArray(event)) {
            this.putArrayBack(event);
        } else {
            this.putItemBack(event);
        }
    }

    /* helper method to put array items back into their original arrays */
    putArrayBack(multipleItems) {
        for (const item of multipleItems) {
            if (this.foodReplica.includes(item) && !this.foods.includes(item)) {
                this.foods.push(item);
                this.foods = this.foods.filter((items, index) => {
                    return this.foods.indexOf(items) === index;
                });
            } else if (this.beachReplica.includes(item) && !this.beach.includes(item)) {
                this.beach.push(item);
                this.beach = this.beach.filter((items, index) => {
                    return this.beach.indexOf(items) === index;
                });
            }
        }
    }

    /* helper method to put single array item back to original array */
    putItemBack(singleItem) {
        if (this.foodReplica.includes(singleItem.toString()) && !this.foods.includes(singleItem.toString())) {
            this.foods.push(singleItem);
            this.foods = this.foods.filter((item, index) => {
                return this.foods.indexOf(item) === index;
            });
        } else if (this.beachReplica.includes(singleItem.toString()) && !this.beach.includes(singleItem.toString())) {
            this.beach.push(singleItem);
            this.beach = this.beach.filter((item, index) => {
                return this.beach.indexOf(item) === index;
            });

        }

    }


    /*** private methods ***/
    private foodOnReload() {
        this.users$.forEach(items => {
            this.foods = this.foods.filter((foodItems) => {
                return !foodItems.includes(items);
            });
        });
    }

    private BeachOnReload() {
        this.users$.forEach(items => {
            this.beach = this.beach.filter((beachItems) => {
                return !beachItems.includes(items);
            });
        });
    }

    private getFoodData(): Observable<Object> {
        return this.getItemsService.getItems(this.foodURL)
            .pipe(
                filter(items => items !== undefined));
    }

    private getBeachData(): Observable<Object> {
        return this.getItemsService.getItems(this.beachURL)
            .pipe(
                filter(items => items !== undefined));
    }

    /*** private methods end here ***/


}
