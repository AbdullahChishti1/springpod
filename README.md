# SpringPod

This project was created by Abdullah Chishti.

- Created a wish list where a user can drop items from both the lists to the wish list

- Items can not be dragged between the food list and the beach list - by design

- Clear all button vanishes when there are no items left in the wish list - by design

- The data in the lists persists on tab close or refresh

- items can be deleted from the wish list which are then sent to their original arrays



#Tools used

- Angular Material has been used.
- Json server has been used to mock API calls

# How to run json-server

After npm install, write json-server db.json in the terminal and the server will start
